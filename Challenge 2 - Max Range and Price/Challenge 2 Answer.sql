#create temporary table t1
# first found the max range of each time period then found the max range of each day 
Select `Date`, max(`rng`) as 'maxRange' from (SELECT `Date`,abs(`Open` - `Close`) As rng FROM `sqlch1`.`trades` trades) as r2
group by `Date`;

#create temporary table t2
# found the max high value for each day
Select `Date`, max(`High`) as maxHigh from trades
group by `Date`;

#create temporary table t3
# found the time of the maxHigh by joining on date and maxHigh
Select t2.date, trades.time, t2.maxHigh from t2
Inner Join trades on (trades.date = t2.date and trades.high = t2.maxHigh)

#Joined the maxHigh and maxRange tables on the dates
Select t3.date, t1.maxRange as 'Range', t3.time as 'Time of Max Price' From t3
Inner Join t1 on t3.date = t1.date
Order by t1.maxRange desc
Limit 3;









