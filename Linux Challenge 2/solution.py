## This is written in python 2.7.12
import time

def getInfo(recordType='Trade',fields=['wTradePrice','wTradeVolume']):
    ## STEP 1: Read in the data file and keep track of where each new record starts
    fieldLen = len(fields) + 2 #number of fields we want to get plus 2 for the record publish and record type lines
    lines = []
    recordLocs = []
    with open('opra_example_regression.log','r') as f:
        for idx,line in enumerate(f):
            lines.append(line)
            if "Record Publish:" in line:
                recordLocs.append(idx)

    ## STEP 2: Find the records that having trading information inside
    tradeLocs = []
    for i in xrange(len(recordLocs)):
        if ("Type: " + str(recordType)) in lines[recordLocs[i] + 1]:
            tradeLocs.append(recordLocs[i])

    ## STEP 3: Find the necessary information and add to trades list
    # The for loop goes through all the locations and then uses an inner for loop between two locations to find the volume and price
    trades = []
    for i in xrange(len(tradeLocs) - 1):
        temp = []
        temp.append(lines[tradeLocs[i]])     #Record publish
        temp.append(lines[tradeLocs[i] + 1]) #Record Type
        trades.append(temp)

        for j in xrange(tradeLocs[i],tradeLocs[i+1]):
            for field in fields:
                if str(field) in lines[j]:
                    temp.append(lines[j])
            if (len(temp) == fieldLen):
                break

    #there is an edge case where the last position does not have a next position to reference to so it must be done manually
    temp = []
    temp.append(lines[tradeLocs[-1]])     #Record publish
    temp.append(lines[tradeLocs[-1] + 1]) #Record Type

    for j in xrange(tradeLocs[-1],len(lines)-1):
        for field in fields:
            if str(field) in lines[j]:
                temp.append(lines[j])
        if (len(temp) == fieldLen):
            break
    ## STEP 4: Clean up text and write output to file
    for trade in trades:
        for i in range(len(trade)):
            trade[i] = trade[i].replace('Regression:',"")
        trade[0] = " ".join(trade[0].split())

    with open('Output.txt','w') as f:
        for trade in trades:
            f.write("".join(trade))

record = 'Quote'
fields=['wMsgType','wPubId']
getInfo(record,fields)
