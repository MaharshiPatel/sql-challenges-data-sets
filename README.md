# Introduction
This repo contains all of the product support training exercises I did.

## SQL Challenge 1
This was an SQL challenge that required us to take the data and 
calculate the volume weighted average price of a ticker. It also 
required us to make it reuseable and user-friendly.

**USER INPUTS**
1. *Start Time*: YYYY-MM-DD HH:MM:SS
2. *End Time*: YYYY-MM-DD HH:MM:SS
3. *Ticker*: The ticker of the particular stock you are trying to 
find

## SQL Challenge 2
This was another SQL challenge that required us to find the maximum 
range in a price of a stock and the maximum price of a stock and give 
the three highest values in the given data. 

The maximum range is calculated by first calculating all ranges for 
each minute and then finding the max of the maximus for each given day. 

The maximum high is calculated by first finding the max high of each 
day. Then the time of the max is calculated by joining the max high 
table with the original dataset with both the date and maxHigh being the 
joining references. 

Then the maxHigh/time table is joined with the maxRange table with the 
date as a reference to create the final output, which is limited to the 
top 3 results. Repeat values may occur.  

## Scripting Challenge 1
Basically a read in/modification/data extraction challenge. nothing too 
over thr top. all commented out for exact process in the source code


## Scripting Challenge 2
This challenge required the script to parse logging data for trades. The 
logs were stored in the forms of records. There were many record types 
including: quotes and trades. Each record had a variable amount of 
fields.

Structure: *Log Data* --> *Records* --> *Trades,Quotes,etc.* --> 
*Fields*

The code essentially parses for record blocks then inside that parses 
for trade blocks and then inside of those blocks it parses for the 
required info and then writes it to an output file. 
 
