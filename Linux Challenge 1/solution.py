# runs on python 2.7.12

import re
import json

# STEP 0: READ IN FILE
# read in text file as different lines
with open('ch1.txt','r') as f:
    lines = f.readlines()

## STEP 1: REMOVE AlL COMENTS FROM THE FILE
#split lines at the # symbol and if the split results in something longer than 1 element than take out the last element where the comment is 
for i in range (0,len(lines)):
    temp = lines[i].split("#")
    if len(temp) > 1:
        temp[-1] = '\n'
    lines[i] = "".join(temp)

## STEP 2: FIND ALL MAC ADDRESSES AND FIND THEIR FREQUENCY
# first join all lines and use regex pattern to find all mac addresses
fileStr = "".join(lines)
macs = re.findall(r'[0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2}',fileStr)

#store all mac addresses in a dictionary to calculate frequency --> better solution for larger files then inner loops
d = {}
for mac in macs:
    if mac in d:
        d[mac] +=1
    else:
        d[mac] = 1
# write mac address to file
with open('MAC Addresses.txt','w') as f:
    f.write("MAC ADDRESSES\tFREQUENCY\n")
    for key in d:
        f.write(str(key) +'\t'+str(d[key])+'\n')

## STEP 3: REMOVE ALL EXTRANEOUS WHITE SPACE AND WRITE IP ADDRESS TO A NEW FILE
# find all IP addresses and write them to a file using an ip address regex patern
ips = re.findall(r'[0-9]*[.][0-9]*[.][0-9]*[.][0-9]*',fileStr)
with open("IP ADDRESSES.txt",'w') as f:
    f.write("ALL IP ADDRESSES FOUND\n")
    for ip in ips:
        f.write(ip+'\n')
# remove all extraneous space and replace with -
with open('Final Output.txt','w') as f: 
    for line in lines:
        f.write(" ".join(line.split()) + '\n')
    

