-- enter in your start and end time in YYYY-MM-DD HH:MM:SS(24 HR TIME) to use the query
-- enter in the your specified ticker as well


SET @StartTime = '2010-10-11 09:00:00';
SET @EndTime = '2010-10-11 14:00:00';
Set @Ticker = 'AAPL';

SELECT `<ticker>` as Ticker, FORMAT (sum(`<vol>` * (`<low>` + `<high>` + `<close>`)/3) / sum(`<vol>`),2) as VWAP, @StartTime as 'Start Time', @EndTime as 'End Time'
FROM `sqlch1`.`data`
where (`<date>` between @StartTime and @EndTime) and `<ticker>` = @Ticker
group by `<ticker>`;

